class Car {
  weels: string;
  doors: number;
  speed: number;
  model: string;

  constructor(weels: string, doors: number, speed: number, model: string) {
    this.weels = weels,
      this.doors = doors,
      this.speed = speed,
      this.model = model
  }
  getModel(): void {
    console.log(this.model)
  }
  MoveonRoad(): void {
    if (this.weels == 'carweels') {
      console.log('Go! We are ride on a road!');
    } else {
      console.log("Oh,sorry we are can't ride on a rails, because we are in a car :(");
    }
  }
}

class Train {
  weels: string;
  model?: string;
  constructor(weels: string, model?: string) {
    this.weels = weels,
      this.model = model
  }
  MoveonRails(): void {
    if (this.weels == 'trainweels') {
      console.log('Go! We are a train!')
    } else {
      console.log('Oh error...')
    }
  }
}
const MyCar = new Car('carweels', 4, 250, 'BMW');
//MyCar.getModel();
//MyCar.MoveonRoad()
const MyTrain = new Train('trainweels')

//_______Way__on rails______

function Car_TO_Train_Adapter(carclass: any, trainclass: any): void {
  console.log('carclass.weels:' + carclass.weels)
  carclass.weels = trainclass.weels
  console.log('carclass.weels:' + carclass.weels)
  if (carclass.weels == 'trainweels') {
    console.log('We are ready moving on a rails!')
  } else {
    console.log('ERROR')
  }
}
//console.log(MyTrain.weels)
Car_TO_Train_Adapter(MyCar, MyTrain)